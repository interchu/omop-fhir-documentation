import json
import yaml
import sys
import glob


with open("Search/implementedFields.yaml") as f:
    data = yaml.load(f, Loader=yaml.FullLoader)


# generate hapi fhir properties
# res = ''
# globalFields = ''
# for resource, fields in data.items():
#     if resource != "Global":
#         res += "\n"+ resource.lower() + ".implementedFields=" + globalFields
#         for field, type in fields[0].items():
#             res+=field + ","
#     else:
#         for field, type in fields[0].items():
#             globalFields += field + ","
# 
# print(res)

# generate the search tables
res = ''
tg = ''
for resource, fields in data.items():
    if resource != "Global":
        t = '<table class="list">\n<tr><td><b>Name</b></td><td><b>Type</b></td><td><b>Description</b></td><td><b>Expression</b></td></tr>\n'
        t += tg
        for field, type in fields[0].items():
            t += '<tr><td><a name="%s"> </a>%s </td><td><a href="https://www.hl7.org/fhir/search.html#%s">%s</a></td><td>%s</td><td>%s</td></tr>\n' % (field , field, type['type'], type['type'], type['descr'], type['expr'])
        t += '</table>\n'
        js = open('IG/pages/_includes/%s-searchtable.md' % resource, "w")
        js.write(str(t))
        js.close()
    else:
        for field, type in fields[0].items():
            tg += '<tr><td><a name="%s"> </a>%s </td><td><a href="https://www.hl7.org/fhir/search.html#%s">%s</a></td><td>%s</td><td>%s</td></tr>\n' % (field , field, type['type'], type['type'], type['descr'], type['expr'])


