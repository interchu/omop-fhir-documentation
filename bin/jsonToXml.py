from dicttoxml import dicttoxml
from xml.dom.minidom import parseString
import json
import sys


filename = sys.argv[1]


with open( filename) as f:
	data = json.load(f)

xdata = dicttoxml(data, custom_root='CodeSystem', attr_type=True)
dom = parseString(xdata)
print(dom.toprettyxml())
