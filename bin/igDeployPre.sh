fold="
ConceptCode
ConceptMap
ValueSet
Binary
Claim
Composition
Condition
DocumentReference
Organization
Patient
Procedure
Encounter
Group
"

for f in $fold
do
  echo "doing: $f"
  b=$(echo $f | tr '[:upper:]' '[:lower:]')
  cp Example/$f/1.json IG/examples/$b-example1.json
  
  touch IG/pages/_includes/${f}EDS-intro.md
  touch IG/pages/_includes/${f}EDS-search.md
  touch IG/pages/_includes/${f}EDS-searchtable.md
  touch IG/pages/_includes/${f}EDS-summary.md
done

cp StructureDefinition/* IG/resources/

ext="
fr-condition-rank
geolocation
"

for f in $ext
do
  echo "doing: $f"
  touch IG/pages/_includes/${f}-intro.md
  touch IG/pages/_includes/${f}-search.md
  touch IG/pages/_includes/${f}-searchtable.md
  touch IG/pages/_includes/${f}-summary.md
done
