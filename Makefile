update-example:
	python bin/buildExample.py ConceptCode
	python bin/buildExample.py ConceptMap
	python bin/buildExample.py ValueSet
	python bin/buildExample.py Binary
	python bin/buildExample.py Claim
	python bin/buildExample.py Composition
	python bin/buildExample.py Condition
	python bin/buildExample.py DocumentReference
	python bin/buildExample.py Organization
	python bin/buildExample.py Patient
	python bin/buildExample.py Procedure
	python bin/buildExample.py Encounter
	python bin/buildExample.py Group

update-example-ig:
	sh bin/igDeployPre.sh

update-fields:
	python bin/implementedToProperties.py

